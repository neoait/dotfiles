# My dotfiles

The programs that i use are mostly terminal based and uses the vi keyset.

## List of programs
* mpd - minimal music player deamon
* mpv - minimal media player
* ncmpcpp - ncurses interface for mpd
* neovim - xdg compatible version of vim
* newsboat - terminal rss-feed reader
* tuir - terminal reddit viewer
* bspwm - wm (do not currently use)
* sxhkd - kb keybinding program
* zsh - shell prompt
* lynx - terminal web browser
* pulsemixer - pulseaudio ncurses interface
* nnn - terminal file manager
* qutebrowser - vim like browser (I use for the most part firefox)
