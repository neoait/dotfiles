#!/bin/sh

# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# Use $XINITRC variable if file exists.
[ -f "$XINITRC" ] && alias startx="startx $XINITRC"

# sudo not required for some system commands
for x in mount umount pacman updatedb ; do
	alias $x="sudo $x"
done

# If filename is typed in, open in program
alias -s \
	{mkv,mp4}="mpv"

# Verbosity and settings that you pretty much just always are going to want.
alias \
	bat="cat /sys/class/power_supply/cw2015-battery/capacity" \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
	bc="bc -ql" \
	mkd="mkdir -pv" \
	yt="youtube-dl --add-metadata -i" \
	yta="yt -x -f bestaudio/best" \
	ffmpeg="ffmpeg -hide_banner"

# Colorize commands when possible.
alias \
	ls="ls -hN --color=auto --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi"

# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	g="git" \
	la="ls -A" \
	ll="ls -lA" \
	trem="transmission-remote" \
	r="tuir -s" \
	lsf="tuir -s livestreamfail" \
	sdn="sudo shutdown -h now" \
	f="$FILE" \
	e="$EDITOR" \
	v="$EDITOR" \
	z="zathura" \
	p="sudo pacman" \
	SS="sudo systemctl"

# Some other stuff
alias \
	magit="nvim -c MagitOnly" \
	tmux="tmux -f $XDG_CONFIG_HOME/tmux/tmux.conf" \
	doom="doom --doomdir $XDG_CONFIG_HOME/doom" \
	nvidia-settings="nvidia-settings --config=$XDG_CONFIG_HOME/nvidia/settings" \
	abook="abook --config "$XDG_CONFIG_HOME"/abook/abookrc --datafile "$XDG_DATA_HOME"/abook/addressbook" \
        wget="wget --hsts-file=$XDG_CACHE_HOME/wget-hsts"

vic() { file="$(which $*)" && $EDITOR $file || echo "$file"; }
mpva() { mpv --ytdl-format=bestaudio --force-window=no ytdl://ytsearch:"$*"; }
mpvv() { mpv ytdl://ytsearch:"$*"; }
