# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# Imports to substitute
from PyQt5.QtCore import QUrl
from qutebrowser.api import interceptor
import re

config.load_autoconfig(False)

# Colors (I only use 3 colors)
background = "#111111"
foreground = "white"
accent = "#ee2222"

# Keybindings
#-----------------------------------------------------------------#

# unbind some stupid bindings
config.unbind('q')
config.unbind('B')
config.unbind('ad')
config.unbind('<Ctrl-v>')

# qute-pass binding
config.bind('za', 'spawn --userscript qute-pass')
config.bind('zu', 'spawn --userscript qute-pass --username-only')
config.bind('zp', 'spawn --userscript qute-pass --password-only')
config.bind('zo', 'spawn --userscript qute-pass --otp-only')

# General binding
config.bind(';m', 'hint links spawn --detach mpv {hint-url}')
config.bind(';H', 'hint links spawn --detach dmenuhandler {hint-url}')
config.bind('<Ctrl-m>', 'spawn --detach mpv {url}')
config.bind('<escape>', 'leave-mode', mode='passthrough')
config.bind('q', 'set-cmd-text -s :quickmark-load')
config.bind('Q', 'set-cmd-text -s :quickmark-load -t')
config.bind('b', 'set-cmd-text -s :bookmark-load')
config.bind('B', 'set-cmd-text -s :bookmark-load -t')
config.bind('e', 'edit-text')
config.bind('E', 'edit-url')
config.bind('tb', 'config-cycle -t statusbar.show always never', mode='normal')
config.bind('tt', 'config-cycle -t tabs.show never always')
#config.bind('a', 'set tabs.favicons.scale 1.3 ;; set tabs.position bottom ;; set tabs.show always ;; set tabs.padding "{bottom: 5, left: 5, right: 5, top: 5}" ;; later 5000 set tabs.favicons.scale 2 ;; later 5000 set tabs.show switching ;; later 5000 set tabs.position left ;; later 5000 set tabs.padding "{bottom: 15, left: 0, right: 5, top: 15}"')
config.bind('a', 'set tabs.show always ;; later 5000 set tabs.show switching')


# Make half-page not require ctrl (tab-close has also a default of <Ctrl-W>)
config.unbind('d')
config.unbind('u')
config.bind('d', 'scroll-page 0 0.5')
config.bind('u', 'scroll-page 0 -0.5')
config.bind('U', 'undo')

config.unbind('xo')
config.unbind('xO')
config.bind('x', 'tab-close')


#-----------------------------------------------------------------#


c.editor.command = ["st", "-e", "nvim", "{file}", "-c", "normal {line}G{column0}l"]

# Use nnn for file selection
c.fileselect.handler = 'external'
c.fileselect.multiple_files.command = ["st", "-e", "nnn", "-p", "{}"]
c.fileselect.single_file.command = ["st", "-e", "nnn", "-p", "{}"]


# Minimize fingerprinting / tracking
#c.content.host_blocking.lists = [
#        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters-2020.txt',
#        'https://easylist.to/easylist/easylist.txt',
#        'https://easylist.to/easylist/easyprivacy.txt',
#        'https://secure.fanboy.co.nz/fanboy-annoyance.txt',
#        '/home/ait/.config/qutebrowser/blocklist'
#]

c.content.headers.user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36"
c.content.canvas_reading = False
c.content.private_browsing = True
c.content.geolocation = False
c.content.notifications.enabled = False
c.content.cookies.accept = 'no-3rdparty'


#   - smart: Search case-sensitively if there are capital characters.
c.search.ignore_case = 'smart'


# Tabs option

## Tabs to the side
#c.tabs.position = 'left'
#c.tabs.width = 50
#c.tabs.favicons.scale = 2
#c.tabs.padding = {"bottom": 15, "left": 0, "right": 5, "top": 15}
#c.tabs.indicator.padding = {"bottom": 0, "left": 0, "right": 10, "top": 0}

## Tabs under
c.tabs.position = 'bottom'
c.tabs.padding = {"bottom": 4, "left": 5, "right": 5, "top": 4}

c.tabs.tabs_are_windows = True
c.tabs.last_close = 'close'
c.tabs.show = 'never'   # switching/multiple
# c.tabs.title.format = ""
# c.tabs.max_width = 30


# Statusbar
c.statusbar.show = 'never' # in-mode, never, always
c.statusbar.padding = {"bottom": 5, "left": 5, "right": 5, "top": 5}

# Completion settings
c.completion.show = 'auto'
c.completion.scrollbar.width = 0
c.completion.shrink = True
c.completion.use_best_match = True
c.completion.open_categories = ["history", "searchengines", "filesystem"]

# Browser settings
c.content.fullscreen.window = True
c.content.javascript.enabled = True
c.colors.webpage.preferred_color_scheme = "dark"
c.scrolling.smooth = True
c.content.user_stylesheets = '~/.config/qutebrowser/lol.css'
c.url.default_page = "https://start.duckduckgo.com/?kak=-1&kal=-1&kao=-1&kaq=-1&kae=d&k7=111&kj=111&kw=n&km=m"
c.url.start_pages = "https://start.duckduckgo.com/?kak=-1&kal=-1&kao=-1&kaq=-1&kae=d&k7=111&kj=111&kw=n&km=m"

# Set the spellchecking
c.spellcheck.languages = ["en-GB", "nb-NO"]

# Setting dark mode
config.set("colors.webpage.darkmode.enabled", True)
c.colors.webpage.bg  = background

# Hints
#c.hints.chars = "AaSsDdFfGgHhJjKkLlQqWwEeRrTtYyUuIiOoPp{[}]|\ZzXxCcVvBbNnMm<,>.?/!123$4%5^6&7*8(9)0_-+=@~#:;"
#c.hints.chars = "asdfghjklqwertyuiop[]\zxcvbnm,./1234567890-=#;"
#c.hints.chars = "abcdefghijklmnopqrstuvwxyz"
c.hints.chars = "asdfjkl;"
c.hints.padding = {"bottom": 2, "left": 2, "right": 2, "top": 2}
c.hints.border = "1px solid " + accent

# Colors
c.colors.statusbar.normal.bg = background
c.colors.statusbar.private.bg = background
c.colors.statusbar.command.bg = background
c.colors.statusbar.command.private.bg = background
c.colors.tabs.selected.odd.bg = accent
c.colors.tabs.selected.even.bg = accent
c.colors.tabs.odd.bg = background
c.colors.tabs.even.bg = background
c.colors.tabs.bar.bg = background
c.colors.messages.info.bg = background
c.colors.messages.error.bg = accent
c.colors.prompts.bg = background
c.colors.completion.item.selected.bg = accent
c.colors.completion.item.selected.border.bottom = accent
c.colors.completion.item.selected.border.top = accent
c.colors.completion.item.selected.fg = foreground
c.colors.completion.item.selected.match.fg = background
c.colors.completion.odd.bg = background
c.colors.completion.even.bg = background
c.colors.completion.category.border.bottom = accent
c.colors.completion.category.border.top = accent
c.colors.completion.category.bg = accent
c.colors.downloads.bar.bg = background
c.colors.downloads.error.bg = accent
c.colors.downloads.system.bg = "none"
c.colors.downloads.system.fg = "none"
c.colors.hints.bg = background
c.colors.hints.fg = foreground
c.colors.hints.match.fg = accent
c.colors.contextmenu.menu.bg = background
c.colors.contextmenu.selected.bg = accent


c.url.searchengines = {
"DEFAULT": "https://duckduckgo.com/?q={}",
#"DEFAULT": "https://search.snopyta.org/?q={}",
#"DEFAULT": "https://www.startpage.com/do/dsearch?query={}",
"s": "https://www.startpage.com/do/dsearch?query={}",
"aw": "https://wiki.archlinux.org/?search={}",
"gw": "https://wiki.gentoo.org/?search={}",
"g": "https://encrypted.google.com/search?q={}",
"y": "http://www.youtube.com/results?search_query={}",
"r": "https://www.reddit.com/r/{}",
"gg": "https://www.google.co.uk/search?&q={}",
"ggi": "https://www.google.co.uk/search?q={}&tbm=isch",
"w": "https://en.wikipedia.org/w/index.php?search={}",
"st": "http://store.steampowered.com/search/?term={}",
"mw": "http://en.uesp.net/w/index.php?title=Special%3ASearch&search={}",
"aur": "https://aur.archlinux.org/packages/?O=0&K={}",
"pac": "https://www.archlinux.org/packages/?sort=&arch=x86_64&maintainer=&flagged=&q={}",
"nh": "https://nethackwiki.com/wiki/index.php?search={}",
"i": "http://www.imdb.com/find?ref_=nv_sr_fn&s=all&q={}",
"dick": "https://en.wiktionary.org/wiki/{}",
"ety": "http://www.etymonline.com/index.php?allowed_in_frame=0&search={}",
"u": "http://www.urbandictionary.com/define.php?term={}",
"y": "https://www.youtube.com/results?search_query={}",
"it": "https://itch.io/search?q={}",
"tpb": "https://www.magnetdl.com/search/?m=1&q={}",
"p": "https://www.protondb.com/search?q={}",
"x": "https://search.snopyta.org/?q={}",
"a": "https://smile.amazon.co.uk/s/?url=search-alias&field-keywords={}",
"gt": "https://translate.google.com/#view=home&op=translate&sl=auto&tl=en&text={}",
"trig": "https://trigedasleng.net/search?q={}",
"rt": "https://www.rottentomatoes.com/search?search={}",

}

# Urls to substitute (should be formated like this: "www.twitter.com": "www.nitter.net",)
urlsubstitutes = {
"www.twitter.com": "https://nitter.snopyta.org/",
"www.reddit.com": "www.libredd.it",
"www.youtube.com": "https://invidious.snopyta.org/",
"www.instagram.com": "https://bibliogram.snopyta.org/",
}

# function to substitutes url
def intercept(info: interceptor.Request):
    if re.match(r'^file://', info.request_url.host()) is not None:
        pass
    else:
        for i in urlsubstitutes:
            if i.find(info.request_url.host()) != -1:
                new_url = QUrl(info.request_url)
                new_url.setHost(urlsubstitutes[i])
                try:
                     info.redirect(new_url)
                except interceptors.RedirectFailedException:
                    pass


#interceptor.register(intercept)
